#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include <windows.h>
#include <GL/glut.h>

#define pi (2*acos(0.0))


void drawAxes()
{
    glBegin(GL_LINES);{
        glVertex3f( 100,0,0);
        glVertex3f(-100,0,0);

        glVertex3f(0,-100,0);
        glVertex3f(0, 100,0);

        ///we are drawing the Z axis too.
        glVertex3f(0,0, 100);
        glVertex3f(0,0,-100);
    }glEnd();
}




void keyboardListener(unsigned char key, int x,int y){

}


void specialKeyListener(int key, int x,int y){

}


void mouseListener(int button, int state, int x, int y){	//x, y is the x-y of the screen (2D)

}

void draw(){
    ///Let's draw some lines
    drawAxes();
    glBegin(GL_LINES);{
        glVertex3f(10, -50, 0);
        glVertex3f(50, -50, 0);
    }glEnd();

    ///Let's draw some triangles
    glBegin(GL_TRIANGLES);{
        glVertex3f(0,0,0);
        glVertex3f(-30, 0, 0);
        glVertex3f(-30, 40, 0);

        ///we can also use glVertex2f or glVertex2d for 2D drawing
        glVertex2f(0, 0);
        glVertex2f(0, -40);
        glVertex2f(-20, -40);

    } glEnd();

    ///Let's draw a square
    glBegin(GL_QUADS);{
        glVertex2f(0, 0);
        glVertex2f(20, 0);
        glVertex2f(20, 20);
        glVertex2f(0, 20);

    } glEnd();

    ///Let's draw an arrow

    /**
        ----|>
        1. Draw a horizontal line
        2. Draw an isosceles triangle at the end of the line
    */
    glBegin(GL_LINES);{
        glVertex2f(0, 0);
        glVertex2f(50, 0);
    } glEnd();
    glBegin(GL_TRIANGLES);{
        glVertex2f(50,10);
        glVertex2f(50, -10);
        glVertex2f(70, 0);

    }glEnd();

}



void display(){

	//clear the display
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0,0,0,0);	//color black
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/********************
	/ set-up camera here
	********************/
	//load the correct matrix -- MODEL-VIEW matrix
	glMatrixMode(GL_MODELVIEW);

	//initialize the matrix
	glLoadIdentity();

	//now give three info
	//1. where is the camera (viewer)?
	//2. where is the camera looking?
	//3. Which direction is the camera's UP direction?



    ///try running the code with different camera parameters and figure out why the images look these ways
    ///for 2d view
    gluLookAt(0,0,200,	0,0,0,	0,1,0);
    ///for 3d view - comment out the above line, and uncomment the following line
    //gluLookAt(100,100,100,	0,0,0,	0,0,1);

	//again select MODEL-VIEW
	glMatrixMode(GL_MODELVIEW);

	/****************************
	/ Add your objects from here
	****************************/
	//add objects
	draw();


	//ADD this line in the end --- if you use double buffer (i.e. GL_DOUBLE)
	glutSwapBuffers();
}


void animate(){
	//codes for any changes in Models, Camera
	glutPostRedisplay();
}

void init(){
	//codes for initialization
	//clear the screen
	glClearColor(0,0,0,0);

	/************************
	/ set-up projection here
	************************/
	//load the PROJECTION matrix
	glMatrixMode(GL_PROJECTION);

	//initialize the matrix
	glLoadIdentity();

	//give PERSPECTIVE parameters
	gluPerspective(80,	1,	1,	1000.0);
	//field of view in the Y (vertically)
	//aspect ratio that determines the field of view in the X direction (horizontally)
	//near distance
	//far distance
}

int main(int argc, char **argv){
	glutInit(&argc,argv);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(0, 0);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);	//Depth, Double buffer, RGB color

	glutCreateWindow("My OpenGL Program");

	init();

	glEnable(GL_DEPTH_TEST);	//enable Depth Testing

	glutDisplayFunc(display);	//display callback function
	glutIdleFunc(animate);		//what you want to do in the idle time (when no drawing is occuring)

	glutKeyboardFunc(keyboardListener);
	glutSpecialFunc(specialKeyListener);
	glutMouseFunc(mouseListener);

	glutMainLoop();		//The main loop of OpenGL

	return 0;
}
