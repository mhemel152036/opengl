#include <bits/stdc++.h>
#define CAP 300
#define pi (2*acos(0.0))
using namespace std;

void matcopy(double** to, double** from, int r=4, int c=4);
void matmul(double** res, double** mat1, double**mat2, int r1=4, int c1 =4, int c2 = 4);
double** newmat(int r=4, int c=4);
void delmat(double** mat, int r=4, int c=4);
double** matI(int r=4);


void ASSERT(bool cond, const char* str){
    if(!cond){
        cout<<str<<endl;
        cout<<"Aborted"<<endl;
        exit(1);
    }
}


class Stack{
    double*** mats;
    int top;
public:

    Stack(){
        top = 0;
        mats = new double**[CAP];
        for(int k=0; k<CAP; k++){
            mats[k] = newmat();
        }
    }

    void push(double** mat){
        ASSERT(top<CAP, "Stack overrunning");
        matcopy(mats[top], mat);
        top++;
    }

    void pop(double** mat){
        ASSERT(top>0, "Stack underrunning");
        top--;
        matcopy(mat, mats[top]);

    }
    bool empty(){
        return top==0;
    }

    ~Stack(){
        for(int k=0; k<CAP; k++){
            delmat(mats[k]);
        }
        delete[] mats;
        mats = NULL;
    }

};

///-------global variables------------------
ifstream scene;
FILE* stage1, *stage2, *stage3;
double** top;
Stack* stk;



void matcopy(double** to, double** from, int r, int c){
    /**copies matrix "from" into matrix "to".
    r = number of rows
    c = number of columns
    **/
    for(int i=0; i<r; i++)
        for(int j=0; j<c; j++)
            to[i][j] = from[i][j];

}

void matmul(double** res, double** mat1, double** mat2, int r1, int c1, int c2) {
    /**
    multiplies two matrices mat1 and mat2, then save their result in res.
    parameters:
    res - where to store the result of multiplication
    mat1 - 1st operand of multiplication
    mat2 - 2nd operand
    r1 - number of rows in mat1  [optional, default = 4]
    c1 - number of columns in mat1 = number of rows in mat2 (prerequisite of matrix multiplication) [optional, default = 4]
    c2 - number of columns in mat2  [optional, default = 4]
    **/
    double** temp = newmat(r1, c2); ///take a temporary matrix to store the result. Otherwise A = A*B will cause error.
    int i, j, k;


    /**calculate
    temp = mat1*mat2 according to matrix multiplication algorithm
    **/


    ///code here------------------
    for(i=0;i<r1;i++){
        for(j=0;j<c2;j++){
            temp[i][j]=0;
            for(k=0;k<c1;k++){
                temp[i][j]+=mat1[i][k]*mat2[k][j];
            }
        }
    }




    ///-----------------------------
    matcopy(res, temp, r1, c2);  ///copy temporary matrix into result
    delmat(temp); ///free the temporary matrix
}


double** newmat(int r, int c){ ///takes a new matrix --------------
    double** mat = new double*[r];
    for(int i=0; i<r; i++){
        mat[i] = new double[c];
    }
    return mat;
}
void delmat(double** mat, int r, int c){  ///frees the memory of a matrix----------
    for(int i=0; i<r; i++){
        delete[] mat[i];
    }
    delete[] mat;
    mat = NULL;
}

double** matI(int r){  ///creates a new matrix of r*r dimension. r = number of rows = number of columns
    double** mat = newmat(r,r);
    for(int i=0; i<r; i++){
        for(int j=0; j<r; j++)
            if(i==j) mat[i][j] = 1;
            else mat[i][j] = 0;
    }
    return mat;
}


void open_files(){
    scene.open("scene.txt");

    stage1 = fopen("stage1.txt", "w");

}

void close_files(){
    scene.close();
    fclose(stage1);
}

void init(){
    srand(3434); ///seed--
    open_files();
    top = newmat();
    stk = new Stack();
}

void finish(){
    close_files();
    delmat(top);
    delete stk;
}

double rad(double deg_angle){
    return deg_angle*pi/180.0;
}
double deg(double rad_angle){
    return rad_angle*180.0/pi;
}


///-- io

void input_triangle(ifstream& in, double** p1, double** p2, double** p3){
    for(int i=0; i<3; i++)
        in>>p1[i][0];
    p1[3][0] = 1;
    for(int i=0; i<3; i++)
        in>>p2[i][0];
    p2[3][0] = 1;
    for(int i=0; i<3; i++)
        in>>p3[i][0];
    p3[3][0] = 1;
}


void output_triangle(FILE* out, double** p1t, double** p2t, double**p3t){
    for(int i=0; i<3; i++)
        fprintf(out,"%.7lf ", p1t[i][0]);
    fprintf(out, "\n");
    for(int i=0; i<3; i++)
        fprintf(out,"%.7lf ", p2t[i][0]);
    fprintf(out, "\n");
    for(int i=0; i<3; i++)
        fprintf(out,"%.7lf ", p3t[i][0]);
    fprintf(out, "\n\n");
}

int main(){

    init();

    double** I = matI(4);  ///identity matrix 4x4
    double** T = newmat();  ///temporary Transformation Matrix 4x4

    double** p1, **p2, **p3;
    p1 = newmat(4,1);       ///point 1 of triangle -- 4x1 matrix
    p2 = newmat(4,1);       ///point 2 --4x1 matrix
    p3 = newmat(4,1);       ///point 3 --4x1 matrix
    double** p1t, **p2t, **p3t;  ///points after transformation. 4x1 matrix each
    p1t = newmat(4,1);
    p2t = newmat(4,1);
    p3t = newmat(4,1);

    double angle;
   /// stage 1
    matcopy(top, I);  ///top is always the current matrix.
    stk->push(I); ///at first we push I into the stack
    string str;
    while(true){
        scene>>str;  ///read the command from scene.txt
        if(str == "end") break; ///end of file--- (done for you)

        if(str == "push") ///push command. push top into stack  (done for you)
            stk->push(top);
        else if(str=="pop")
            stk->pop(top);  ///pop command. pop top from stack   (done for you)
        else if(str == "translate"){
            double tx, ty, tz;
            scene>>tx>>ty>>tz;  ///read the translate parameters from scene.txt
            /**now we will create the translation matrix Translate(dx, dy, dz)
            T = 1 0 0 tx
                0 1 0 ty
                0 0 1 tz
                0 0 0 1
            */
            ///code here -----------------
            matcopy(T, I);
            T[0][3]=tx;
            T[1][3]=ty;
            T[2][3]=tz;
            //T[0][3]=tx;




            ///---------------------------

            ///current matrix = current matrix * T
            matmul(top, top, T);
        }
        else if(str == "rotate"){
            double ax, ay, az;
            scene>>angle;
            scene>>ax>>ay>>az;



            /**
            Calculate the rotation matrix "T" as Rotate(angle, ax, ay, az)
            */

            ///code here --------------------------------

            double l=sqrt(ax*ax+ay*ay+az*az);
            ax=ax/l;
            ay=ay/l;
            az=az/l;

            matcopy(T, I);



            T[0][0]=ax*ax*(1-cos(rad(angle)))+cos(rad(angle));
            T[0][1]=ax*ay*(1-cos(rad(angle)))-az*sin(rad(angle));
            T[0][2]=ax*az*(1-cos(rad(angle)))+ay*sin(rad(angle));

            T[1][0]=ay*ax*(1-cos(rad(angle)))+az*sin(rad(angle));
            T[1][1]=ay*ay*(1-cos(rad(angle)))+cos(rad(angle));
            T[1][2]=ay*az*(1-cos(rad(angle)))-ax*sin(rad(angle));

            T[2][0]=az*ax*(1-cos(rad(angle)))-ay*sin(rad(angle));
            T[2][1]=az*ay*(1-cos(rad(angle)))+ax*sin(rad(angle));
            T[2][2]=az*az*(1-cos(rad(angle)))+cos(rad(angle));








            ///----------------------------------------
            matmul(top, top, T); ///top = top*T

        }
        else if(str == "scale"){
            double sx, sy, sz;
            scene>>sx>>sy>>sz;
            /***
            Create the scale matrix T
            T = sx 0  0  0
                0  sy 0  0
                0  0  sz 1
                0  0  0  1
            */
            ///code here --------------------- (scale operation is done for you)
            matcopy(T, I);  ///initialize A as Identity
            T[0][0] = sx;   ///replace the diagonal entries with scale parameters
            T[1][1] = sy;
            T[2][2] = sz;
            ///--------------------------------

            matmul(top, top, T); ///top = top*A
        }
        else if(str == "triangle"){
            input_triangle(scene, p1, p2, p3); ///read triangle coordinates from input file into p1, p2, p3
            ///stage1
            ///modeling transformation-- top = Transform matrix

            /**
            p1t = top*p1
            p2t = top*p2
            p3t = top*p3
            **/
            ///code here ------------------------------
            //matcopy(top,I);
            matmul(p1t,top,p1,4,4,1);
            //matcopy(top,I);
            matmul(p2t,top,p2,4,4,1);
            //matcopy(top,I);
            matmul(p3t,top,p3,4,4,1);




            ///-----------------------------------------------
            output_triangle(stage1, p1t, p2t, p3t);  ///write to the output file
        }

    }

    delmat(I);
    delmat(T);
    delmat(p1);
    delmat(p2);
    delmat(p3);
    delmat(p1t);
    delmat(p2t);
    delmat(p3t);

    finish();

}
