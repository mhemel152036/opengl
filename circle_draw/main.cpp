#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include <windows.h>
#include <GL/glut.h>

#define pi (2*acos(0.0))

struct Point{
    double x;
    double y;
    double z;
};

void drawAxes(){
    glBegin(GL_LINES);{
        glVertex3f( 100,0,0);
        glVertex3f(-100,0,0);

        glVertex3f(0,-100,0);
        glVertex3f(0, 100,0);

        ///we are drawing the Z axis too.
        glVertex3f(0,0, 100);
        glVertex3f(0,0,-100);
    }glEnd();
}
void keyboardListener(unsigned char key, int x,int y){

}
void specialKeyListener(int key, int x,int y){
}
void mouseListener(int button, int state, int x, int y){	//x, y is the x-y of the screen (2D)

}
double f(double x){
    //return pow(x,2);
    return x*x;
}
void drawCircle(int redius,int slice){
    Point points[100];
    int i;
    double theta,x,y;

    for(i=0;i<=slice;i++){
        theta=(2*pi/slice)*i;
        x=redius*cos(theta);
        y=redius*sin(theta);
        points[i].x=10*x; /*scaled by 30 unit*/
        points[i].y=10*y; /*scaled by 30 unit*/
    }
    glBegin(GL_LINES);
    for(i=0;i<slice;i++){
        glVertex3f(points[i].x,points[i].y,0);
        glVertex3f(points[i+1].x,points[i+1].y,0);
    }
    glEnd();
}
void draw(){
    ///Let's draw some lines
    drawAxes();
    drawCircle(5,50); /*call circle parabola function*/

}

void display(){

	//clear the display
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0,0,0,0);	//color black
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/********************
	/ set-up camera here
	********************/
	//load the correct matrix -- MODEL-VIEW matrix
	glMatrixMode(GL_MODELVIEW);

	//initialize the matrix
	glLoadIdentity();

	//now give three info
	//1. where is the camera (viewer)?
	//2. where is the camera looking?
	//3. Which direction is the camera's UP direction?



    ///try running the code with different camera parameters and figure out why the images look these ways
    ///for 2d view
    gluLookAt(0,0,200,	0,0,0,	0,1,0);
    ///for 3d view - comment out the above line, and uncomment the following line
    //gluLookAt(100,100,100,	0,0,0,	0,0,1);

	//again select MODEL-VIEW
	glMatrixMode(GL_MODELVIEW);

	/****************************
	/ Add your objects from here
	****************************/
	//add objects
	draw();


	//ADD this line in the end --- if you use double buffer (i.e. GL_DOUBLE)
	glutSwapBuffers();
}


void animate(){
	//codes for any changes in Models, Camera
	glutPostRedisplay();
}

void init(){
	//codes for initialization
	//clear the screen
	glClearColor(0,0,0,0);

	/************************
	/ set-up projection here
	************************/
	//load the PROJECTION matrix
	glMatrixMode(GL_PROJECTION);

	//initialize the matrix
	glLoadIdentity();

	//give PERSPECTIVE parameters
	gluPerspective(80,	1,	1,	1000.0);
	//field of view in the Y (vertically)
	//aspect ratio that determines the field of view in the X direction (horizontally)
	//near distance
	//far distance
}

int main(int argc, char **argv){
	glutInit(&argc,argv);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(0, 0);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);	//Depth, Double buffer, RGB color

	glutCreateWindow("My OpenGL Program");

	init();

	glEnable(GL_DEPTH_TEST);	//enable Depth Testing

	glutDisplayFunc(display);	//display callback function
	glutIdleFunc(animate);		//what you want to do in the idle time (when no drawing is occuring)

	glutKeyboardFunc(keyboardListener);
	glutSpecialFunc(specialKeyListener);
	glutMouseFunc(mouseListener);

	glutMainLoop();		//The main loop of OpenGL

	return 0;
}
