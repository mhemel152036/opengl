#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include <windows.h>
#include <GL/glut.h>

#define pi (2*acos(0.0))

struct Point {
    double x, y, z;
};

double angle,shift;


void drawAxes()
{
    glBegin(GL_LINES);{
        glColor3f(1,0,0);
        glVertex3f( 100,0,0);
        glVertex3f(-100,0,0);

        glColor3f(0,1,0);
        glVertex3f(0,-100,0);
        glVertex3f(0, 100,0);

        ///we are drawing the Z axis too.
        glColor3f(0,0,1);
        glVertex3f(0,0, 100);
        glVertex3f(0,0,-100);
    }glEnd();
    glColor3f(1,1,1);
}




void keyboardListener(unsigned char key, int x,int y){

}


void specialKeyListener(int key, int x,int y){

}


void mouseListener(int button, int state, int x, int y){	//x, y is the x-y of the screen (2D)

}

double f(double x){
    return sin(x);
}

void drawCurve(double startX, double endX, int segments){
    int i;
    double x, y;
    struct Point points[100];
    for(i=0; i<=segments; i++){
        x = startX + (double) i/ (double) segments * (endX - startX);
        y = f(x);
        points[i].x = x;
        points[i].y = y;
    }

    glBegin(GL_LINES);
    for(i=0; i<segments; i++){
        glVertex3f(points[i].x, points[i].y, 0);
        glVertex3f(points[i+1].x, points[i+1].y, 0);
    }
    glEnd();
}


void drawCircle(double radius,int slices){
    int i;
    struct Point points[100];
    double theta;
    /**
    We generate the points at first--
    for i-th slice, theta = i*360/slices
                            or i*2*pi/slices (in radian)
    x = r*cos(theta)
    y = r*cos(theta)
    then connect every point to its next one with a straight line
    */
    for(i=0; i<= slices; i++){
        theta = (double) i / (double) slices * 2*pi;
        points[i].x = radius* cos(theta);
        points[i].y = radius* sin(theta);
    }
    /**
    Now connect the points
    */
    glBegin(GL_LINES); {
    for(i =0; i<slices; i++){
        glVertex3f(points[i].x, points[i].y, 0);
        glVertex3f(points[i+1].x, points[i+1].y, 0);
    }
    } glEnd();
}

void drawSquare(double a){
glBegin(GL_QUADS);{
        glVertex2d(-a, -a);
        glVertex2d(-a, a);
        glVertex2d(a, a);
        glVertex2d(a, -a);
    }glEnd();
}
void drawTriangle(double a){

    glBegin(GL_POLYGON);
        glVertex2f(-a,0);
        glVertex2f(0,a);
        glVertex2f(a,0);
    glEnd();

    glColor3f(1,1,0);
    glBegin(GL_LINES);
        glVertex2f(0,0);
        glVertex2f(0,-20);
    glEnd();


}
void drawSS()
{
    ///glColor3f(1,0,0); ///color of sun
    glPushMatrix();{
        drawCircle(100,50); ///sun

        glRotatef(angle,0,0,1);
        glTranslatef(100,0,0);
        drawTriangle(10);
    }
    glPopMatrix();


    glScalef(30,30,0);
    glTranslatef(shift,0,0);
    drawCurve(-4*pi,4*pi,100);








    /*glRotatef(angle,0,0,1); ///revolution around sun
    glTranslatef(110,0,0);   ///distance from sun
    glRotatef(2*angle,0,0,1);   ///rotation around own axis
    glColor3f(0,1,0); ///color of planet
    drawSquare(15); ///planet


    glPushMatrix(); ///save the current transformation matrix
    {

    glRotatef(angle,0,0,1); ///revolution around planet
    glTranslatef(60,0,0); ///distance from planet
    glRotatef(2*angle,0,0,1); ///rotation around own axis
    glColor3f(0,0,1); //color of moon
    drawSquare(10); ///moon

    }
    glPopMatrix(); ///restore to the saved transformation matrix

    glRotatef(3*angle,0,0,1); ///revolution around planet
    glTranslatef(40,0,0); ///distance from planet
    glRotatef(4*angle,0,0,1); ///rotation around own axis
    glColor3f(1,1,0); //color of moon2
    drawSquare(5); ///moon2*/

}

void draw(){
    drawAxes();

    /*
    glTranslatef(50, 0, 0);
    glRotatef(45, 0,0,1);
    drawSquare(20);
    */
    drawSS();



}



void display(){

	//clear the display
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0,0,0,0);	//color black
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/********************
	/ set-up camera here
	********************/
	//load the correct matrix -- MODEL-VIEW matrix
	glMatrixMode(GL_MODELVIEW);

	//initialize the matrix
	glLoadIdentity();

	//now give three info
	//1. where is the camera (viewer)?
	//2. where is the camera looking?
	//3. Which direction is the camera's UP direction?



    ///try running the code with different camera parameters and figure out why the images look these ways
    ///for 2d view
    gluLookAt(0,0,200,	0,0,0,	0,1,0);
    ///for 3d view - comment out the above line, and uncomment the following line
    //gluLookAt(100,100,100,	0,0,0,	0,0,1);

	//again select MODEL-VIEW
	glMatrixMode(GL_MODELVIEW);

	/****************************
	/ Add your objects from here
	****************************/
	//add objects
	draw();


	//ADD this line in the end --- if you use double buffer (i.e. GL_DOUBLE)
	glutSwapBuffers();
}


void animate(){
	//codes for any changes in Models, Camera
	angle += 0.01;



    shift+=0.001;
    if(shift>2*pi){
            shift=0;
    }




	glutPostRedisplay();
}

void init(){
	//codes for initialization
	angle = 0;
	//clear the screen
	glClearColor(0,0,0,0);

	/************************
	/ set-up projection here
	************************/
	//load the PROJECTION matrix
	glMatrixMode(GL_PROJECTION);

	//initialize the matrix
	glLoadIdentity();

	//give PERSPECTIVE parameters
	gluPerspective(80,	1,	1,	1000.0);
	//field of view in the Y (vertically)
	//aspect ratio that determines the field of view in the X direction (horizontally)
	//near distance
	//far distance
}

int main(int argc, char **argv){
	glutInit(&argc,argv);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(0, 0);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);	//Depth, Double buffer, RGB color

	glutCreateWindow("My OpenGL Program");

	init();

	glEnable(GL_DEPTH_TEST);	//enable Depth Testing

	glutDisplayFunc(display);	//display callback function
	glutIdleFunc(animate);		//what you want to do in the idle time (when no drawing is occuring)

	glutKeyboardFunc(keyboardListener);
	glutSpecialFunc(specialKeyListener);
	glutMouseFunc(mouseListener);

	glutMainLoop();		//The main loop of OpenGL

	return 0;
}
